from handlers.handler import hello, health_check, send_message_to_sqs, fallback_to_alternate_queues, \
append_and_compress, search_for_old_log, upload_and_cleanup, aggregate_old_logs
import json
import os
from unittest.mock import patch
from botocore.stub import Stubber, ANY
import boto3
import botocore
import gzip
from datetime import datetime, timedelta
from handlers import handler
import tempfile
import pytest
import io
import csv

class TestHandler():

    def setup_class(self):
        # make sure that production is not affected by mistake
        os.environ["AWS_ACCESS_KEY_ID"] = "testing"
        os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
        os.environ["AWS_SECURITY_TOKEN"] = "testing"
        os.environ["AWS_SESSION_TOKEN"] = "testing"

    def teardown_class(self):
        pass

    def test_hello(self):
        with patch("handlers.handler.send_message_to_sqs"):
            response = hello(None, None)
            assert response["statusCode"] == 200
            assert json.loads(response["body"])["message"] == "helloworld"

    def test_health_check_success(self):
        response = health_check(None, None)
        assert response["body"] == '{"status": "ok"}'

    def test_health_check_fail(self):
        os.environ["STATUS"] = "fail"
        response = health_check(None, None)
        assert response["body"] == '{"status": "fail"}'

        del os.environ['STATUS']

        response = health_check(None, None)
        assert response["body"] == '{"status": "ok"}'

        os.environ["status"] = "fail"
        response = health_check(None, None)
        assert response["body"] == '{"status": "fail"}'

        del os.environ['status']

        response = health_check(None, None)
        assert response["body"] == '{"status": "ok"}'

    def test_send_message_to_sqs(self):

        stubber = Stubber(handler.sqs_client)
        stubber.add_response("send_message", {
            'MD5OfMessageBody': 'string',
            'MD5OfMessageAttributes': 'string',
            'MD5OfMessageSystemAttributes': 'string',
            'MessageId': 'string',
            'SequenceNumber': 'string'
        }, 
        expected_params={
            'MessageBody': ANY,
            'QueueUrl': 'test_queue'
        })
        with stubber:
            with patch.object(handler.sqs_client, "send_message", wraps=handler.sqs_client.send_message) as patched_send_message:
                send_message_to_sqs("test_queue", "test_region", "0.0.0.0", "abcd")

    def test_fallback_to_alternate_queues(self):
        os.environ["DEPLOYMENT_REGIONS"] = "us-east-1,us-west-2,us-west-1"
        client = boto3.client('sqs')
        stubber = Stubber(client)
        list_queues_response = {
            'QueueUrls': [
                'string',
            ],
            'NextToken': 'string'
        }
        for i in range(3):
            stubber.add_response("list_queues", list_queues_response, expected_params={
                "QueueNamePrefix": "HelloWorldQueue",
            })
        with stubber:
            with patch.object(boto3, "client", wraps=boto3.client, return_value=client) as mocked_client:
                with patch("handlers.handler.send_message_to_sqs"):
                    attempts = fallback_to_alternate_queues("us-east-1", "0.0.0.0", "abcd")
                    assert attempts == 1
                with patch("handlers.handler.send_message_to_sqs", side_effect=botocore.exceptions.ClientError({"Error": {"Code": "Fake Error"}}, "blah")):
                    attempts = fallback_to_alternate_queues("us-east-1", "0.0.0.0", "abcd")
                    assert attempts == 2


    def test_append_and_compress_existing_file(self):
        rows = [{"requestId": "1", "timeStamp": "2", "region": "3"}]

        event = {
            "Records": [
                {"body": '{"requestId": "one", "timeStamp": "111", "region": "here"}'},
                {"body": '{"requestId": "two", "timeStamp": "333", "region": "there"}'},
            ]
        }
        updated_file = append_and_compress(rows, event)
        with gzip.open(updated_file.name, 'rb') as f_in:
            file_content = f_in.read().decode("utf-8")
            assert file_content == '"region","requestId","timeStamp"\r\n"3","1","2"\r\n"here","one","111"\r\n"there","two","333"\r\n'

    def test_append_and_compress_new_file(self):
        rows = []

        event = {
            "Records": [
                {"body": '{"requestId": "one", "timeStamp": "111", "region": "here"}'},
            ]
        }
        updated_file = append_and_compress(rows, event)
        with gzip.open(updated_file.name, 'rb') as f_in:
            file_content = f_in.read().decode("utf-8")
            assert len(file_content.split()) == 2
            for line in file_content.split():
                assert len(line.split(",")) == 3
            assert len(file_content) > 0

    def test_search_for_old_log_existing_file(self):
        os.environ["INVOCATION_BUCKET"] = "test_bucket"

        stubber = Stubber(handler.s3_client)
        list_objects_v2_response = {  
            'Contents': [
                {'Key': 'tmp/tmp-invocation-log-us-east-1-2021-07-11-10.13212311.gz'},
                {'Key': 'tmp/tmp-invocation-log-us-east-1-2021-07-11-10.13212331.gz'},
            ],
        }
        stubber.add_response("list_objects_v2", list_objects_v2_response, expected_params={
            "Bucket": "test_bucket", 
            "Prefix": "tmp/tmp-invocation-log"
        })
        temp_file = tempfile.NamedTemporaryFile(suffix='.tar.gz')
        with gzip.open(temp_file.name, 'wb') as f_out:
            f_out.write("requestId,timeStamp,region\n3343,34,333\n".encode("utf-8"))

        current_date =datetime(year=2021, month=7, day=11, hour=11)
        with stubber:
            with patch.object(tempfile, "NamedTemporaryFile", return_value=temp_file) as mocked_temp_file:
                with patch.object(handler.s3_client, "download_file", wraps=handler.s3_client.download_file, return_value=None) as patched_download_file:
                    batched_rows, new_keys = search_for_old_log("tmp/tmp-invocation-log", current_date)
        log_date = datetime(year=2021, month=7, day=11, hour=10)
        assert batched_rows[log_date] == [{"requestId": "3343", "timeStamp": "34", "region": "333"}, {"requestId": "3343", "timeStamp": "34", "region": "333"}]
        assert new_keys == ['tmp/tmp-invocation-log-us-east-1-2021-07-11-10.13212311.gz', 'tmp/tmp-invocation-log-us-east-1-2021-07-11-10.13212331.gz']

    def test_search_for_old_log_new_file(self):
        os.environ["INVOCATION_BUCKET"] = "test_bucket"

        stubber = Stubber(handler.s3_client)
        list_objects_v2_response = {  
            'Contents': [
            ],
        }
        stubber.add_response("list_objects_v2", list_objects_v2_response, expected_params={
            "Bucket": "test_bucket", 
            "Prefix": "test_prefix"
        })
        temp_file = tempfile.NamedTemporaryFile(suffix='.tar.gz')

        with stubber:
            with patch.object(tempfile, "NamedTemporaryFile", return_value=temp_file) as mocked_temp_file:
                with patch.object(handler.s3_client, "download_file", wraps=handler.s3_client.download_file, return_value=None) as patched_download_file:
                    rows, keys = search_for_old_log("test_prefix", datetime.utcnow())
        for row in rows:
            raise RuntimeError("there shouldn't be any rows in new file")

    def test_upload_and_cleanup(self):
        os.environ["INVOCATION_BUCKET"] = "test_bucket"
        os.environ["INVOCATION_QUEUE"] = "test_queue"
        target_file = tempfile.NamedTemporaryFile(suffix='.tar.gz')
        stubber = Stubber(handler.sqs_client)
        for i in range(2):
            stubber.add_response("delete_message", {}, expected_params={
                "QueueUrl": "test_queue",
                "ReceiptHandle": "blah_%d" % i,
            })
        with stubber:
            with patch.object(handler.s3_client, "upload_file", wraps=handler.s3_client.upload_file, return_value=None) as patched_upload_file:
                with patch.object(handler.sqs_client, "delete_message", wraps=handler.sqs_client.delete_message) as patched_delete_message:
                    upload_and_cleanup("test_prefix", target_file, {"Records": [{"body": "one", "receiptHandle": "blah_0"}, {"body": "two", "receiptHandle": "blah_1"}]})
                    assert handler.sqs_client.delete_message.call_count == 2

    def test_aggregate_old_logs_last_hour(self):
        os.environ["INVOCATION_BUCKET"] = "test_bucket"
        stubber = Stubber(handler.s3_client)
        list_objects_v2_response = {  
            'Contents': [
                {'Key': "tmp/tmp-invocation-log-us-east-1-2021-07-11-10.2342342.gz"},
                {'Key': "tmp/tmp-invocation-log-us-east-1-2021-07-11-10.2342342.gz"}
            ],
        }
        current_date = datetime(year=2021, month=7, day=11, hour=11)
        stubber.add_response("list_objects_v2", list_objects_v2_response, expected_params={
            "Bucket": "test_bucket", 
            "Prefix": "tmp/tmp-invocation-log-us-east-1"
        })
        delete_objects_response = {
            'Deleted': [
                {'Key': "tmp/tmp-invocation-log-us-east-1-2021-07-11-10.2342342.gz"},
                {'Key': "tmp/tmp-invocation-log-us-east-1-2021-07-11-10.2342342.gz"}
            ],
        }
        delete_objects_expected_params = {
            'Bucket': 'test_bucket',
            'Delete': {
                'Objects': [
                    {'Key': "tmp/tmp-invocation-log-us-east-1-2021-07-11-10.2342342.gz"},
                    {'Key': "tmp/tmp-invocation-log-us-east-1-2021-07-11-10.2342342.gz"}
                ]
            }
        }
        stubber.add_response("delete_objects", delete_objects_response, expected_params=delete_objects_expected_params)
        temp_file = tempfile.NamedTemporaryFile(suffix='.gz')
        with gzip.open(temp_file.name, 'wb') as f_out:
            f_out.write("requestId,timeStamp,region\n3343,34,333\n".encode("utf-8"))

        with stubber:
            with patch.object(tempfile, "NamedTemporaryFile", return_value=temp_file) as mocked_temp_file:
                with patch.object(handler.s3_client, "download_file", wraps=handler.s3_client.download_file, return_value=None) as patched_download_file:
                    with patch.object(handler.s3_client, "upload_file", wraps=handler.s3_client.upload_file, return_value=None) as patched_upload_file:
                        aggregate_old_logs(current_date, "us-east-1")
                        assert patched_upload_file.call_count == 1
                        assert patched_download_file.call_count == 2
                        assert mocked_temp_file.call_count == 4 # twice to fetch the two files, and once for the joined file
        
    def test_aggregate_old_logs_much_older_files(self):
        os.environ["INVOCATION_BUCKET"] = "test_bucket"
        stubber = Stubber(handler.s3_client)
        list_objects_v2_response = {  
            'Contents': [
                {'Key': "tmp/tmp-invocation-log-us-east-1-2021-07-01-10.2342342.gz"},
                {'Key': "tmp/tmp-invocation-log-us-east-1-2021-07-01-10.2342342.gz"},
                {'Key': "tmp/tmp-invocation-log-us-east-1-2021-05-01-10.2342342.gz"},
                {'Key': "tmp/tmp-invocation-log-us-east-1-2021-06-01-10.2342342.gz"}
            ],
        }
        current_date = datetime(year=2021, month=7, day=11, hour=11)
        stubber.add_response("list_objects_v2", list_objects_v2_response, expected_params={
            "Bucket": "test_bucket", 
            "Prefix": "tmp/tmp-invocation-log-us-east-1"
        })
        delete_objects_response = {
            'Deleted': [
                {'Key': 'tmp/tmp-invocation-log-us-east-1-2021-07-01-10.2342342.gz'},
                {'Key': 'tmp/tmp-invocation-log-us-east-1-2021-07-01-10.2342342.gz'},
            ],
        }
        delete_objects_expected_params = {
            'Bucket': 'test_bucket',
            'Delete': {
                'Objects': [
                    {'Key': 'tmp/tmp-invocation-log-us-east-1-2021-07-01-10.2342342.gz'},
                    {'Key': 'tmp/tmp-invocation-log-us-east-1-2021-07-01-10.2342342.gz'},
                ]
            }
        }
        stubber.add_response("delete_objects", delete_objects_response, expected_params=delete_objects_expected_params)


        delete_objects_response = {
            'Deleted': [
                {'Key': "tmp/tmp-invocation-log-us-east-1-2021-05-01-10.2342342.gz"},
            ],
        }
        delete_objects_expected_params = {
            'Bucket': 'test_bucket',
            'Delete': {
                'Objects': [
                    {'Key': "tmp/tmp-invocation-log-us-east-1-2021-05-01-10.2342342.gz"},
                ]
            }
        }
        stubber.add_response("delete_objects", delete_objects_response, expected_params=delete_objects_expected_params)
        delete_objects_response = {
            'Deleted': [
                {'Key': "tmp/tmp-invocation-log-us-east-1-2021-06-01-10.2342342.gz"}
            ],
        }
        delete_objects_expected_params = {
            'Bucket': 'test_bucket',
            'Delete': {
                'Objects': [
                    {'Key': "tmp/tmp-invocation-log-us-east-1-2021-06-01-10.2342342.gz"}
                ]
            }
        }
        stubber.add_response("delete_objects", delete_objects_response, expected_params=delete_objects_expected_params)

        temp_file = tempfile.NamedTemporaryFile(suffix='.gz')
        with gzip.open(temp_file.name, 'wb') as f_out:
            f_out.write("requestId,timeStamp,region\n3343,34,333\n".encode("utf-8"))


        much_older_list_objects_v2_response = {  
            'Contents': [
                {'Key': "tmp/tmp-invocation-log-one"},
                {'Key': "tmp/tmp-invocation-log-two"}
            ],
        }
        stubber.add_response("list_objects_v2", much_older_list_objects_v2_response, expected_params={
            "Bucket": "test_bucket", 
            "Prefix": "tmp/tmp-invocation-log"
        })
        with stubber:
            with patch.object(tempfile, "NamedTemporaryFile", return_value=temp_file) as mocked_temp_file:
                with patch.object(handler.s3_client, "download_file", wraps=handler.s3_client.download_file, return_value=None) as patched_download_file:
                    with patch.object(handler.s3_client, "upload_file", wraps=handler.s3_client.upload_file, return_value=None) as patched_upload_file:
                        aggregate_old_logs(current_date, "us-east-1")
                        assert patched_upload_file.call_count == 3
                        assert patched_download_file.call_count == 4
                        assert mocked_temp_file.call_count == 10 # 4 times to fetch the 4 files, 3 to upload and 3 to upload the joined file
        