#!/bin/sh
# make sure that this entire script fails if any command fails
set -e

echo "echo running Build phase `date`"

[[ -z "$region" ]] && { echo "region parameter is empty" ; exit 1; }

echo "building in region $region"

echo "Creating virtual environment"
python3 -m venv $CODEBUILD_SRC_DIR/venv

. $CODEBUILD_SRC_DIR/venv/bin/activate

echo "Installing dependencies"
pip3 install -r $CODEBUILD_SRC_DIR/requirements/build.txt

echo "Running the unit tests"
# https://docs.aws.amazon.com/codebuild/latest/userguide/test-report-pytest.html
python -m pytest --junitxml=$CODEBUILD_SRC_DIR/pytest_unit_tests.xml

for stage in `echo "gamma prod"`; do
    echo "packaging for stage: $stage, region: $region"
    mkdir -p $CODEBUILD_SRC_DIR/target/$stage
    serverless package --package $CODEBUILD_SRC_DIR/target/$stage --stage $stage -v -r $region
done
deactivate
  
echo "end of the Build phase `date`"