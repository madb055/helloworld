import json
import logging
import os
from datetime import datetime, timedelta
import boto3
import botocore.exceptions
from botocore.config import Config
import gzip
import tempfile
import io
import csv
import re

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)
MAX_RETRIES = 3

# configure the aws client to automatically attempt backoff and retry logic
config = Config(
   retries = {
      'max_attempts': MAX_RETRIES,
      'mode': 'standard'
   }
)

sqs_client = boto3.client('sqs', config=config)
s3_client = boto3.client('s3', config=config)

def hello(event, context):
    """Lambda for processing helloworld API requests"""

    body = {
        "message": "helloworld",
        "input": event
    }
    region = None
    if context:
        region = context.invoked_function_arn.split(':')[3]
        if region:
            logger.info("message: hellowworld from " + region)
            body["message"] = "%s from %s : %s" % (body["message"], os.environ["STAGE"], region)
    else:
        logger.info("message: hellowworld"  )

    # put message on sqs
    ip_address = None
    request_id = None
    if event and "requestContext" in event:
        ip_address = event['requestContext']['identity']['sourceIp']
        request_id = event["requestContext"]["requestId"]
    try:
        if "TEST_SQS_FALLBACK" in os.environ:
            raise botocore.exceptions.ClientError(error_response={"Code": 500}, operation_name="send_message")
        send_message_to_sqs(os.environ["INVOCATION_QUEUE"], region, ip_address, request_id)
    except botocore.exceptions.ClientError as err:
        logger.error(err)
        logger.info("Attempting fallback with alternate sqs queue")
        try:
            attempts = fallback_to_alternate_queues(region, ip_address, request_id)
        except botocore.exceptions.ClientError as err:
            logger.error(err)
        except Exception as err:
            logger.error(err)
    except Exception as err:
        logger.error(err)
        logger.info("Got an exception, but ignoring it")
    return  {
        "statusCode": 200,
        "body": json.dumps(body)
    }

def send_message_to_sqs(sqs_queue, region, ip_address, request_id):
    message = json.dumps({"requestId": request_id, "region": region, "timeStamp": datetime.utcnow().isoformat(), "ipAddress": ip_address})
    try:
        response = sqs_client.send_message(
            QueueUrl=sqs_queue,
            MessageBody=message,
        )
    except botocore.exceptions.ClientError as error:
        logger.error(error)
        logger.error(error.response['Error']['Code'])
        raise error

def fallback_to_alternate_queues(current_region: str, ip_address: str, request_id: str):
    # find all queues by prefix
    # iterate through the queues till you get the first posting
    attempts = 0
    finished_regions = set({})
    for region in os.environ["DEPLOYMENT_REGIONS"].split(","):
        # skip the cuurent queue
        region = region.lower().strip()
        if region == current_region or region in finished_regions:
            finished_regions.add(region)
            logger.info("Skipping current region %s" % region)
            continue
        finished_regions.add(region)
        logger.info("looking for queue in %s" % current_region)
        config = Config(
           retries = {
              'max_attempts': MAX_RETRIES,
              'mode': 'standard'
           },
           region_name=region,
        )
        sqs_client = boto3.client('sqs', config=config)
        response = sqs_client.list_queues(QueueNamePrefix="HelloWorldQueue")
        if len(response["QueueUrls"]) > 0:
            try:
                attempts += 1
                # you should notice contradicting regions in the log entry if a message has been passed from one region to another
                # this is because the "current_region" is forwarded
                send_message_to_sqs(response["QueueUrls"][0], current_region, ip_address, request_id)
                return attempts
            except botocore.exceptions.ClientError as error:
                logger.error("Failed to fallback to %s" % region)
                continue
    if attempts == 0:
        logger.error("Failed to make any attempts to fallback to alternate queues")
        raise RuntimeError("Failed to make any fallback attempts")
    return attempts

def health_check(event, context):
    """Lambda handler for getting the health."""

    # using an environment variable to test the failover at route 53
    if 'STATUS' in os.environ or 'status' in os.environ:
        status = None
        if 'STATUS' in os.environ:
            status = os.environ['STATUS']
        else:
            status = os.environ['status']
        logger.info("status: " + status)
        
        return {
            "statusCode": 200,
            "body": json.dumps({"status": status}),
        }
    else:
        return {
            "statusCode": 200,
            "body": json.dumps({"status": "ok"}),
        }

def process_invocation_record(event, context):
    """Lambda for processing an invocation record"""

    if len(event["Records"]) == 0:
        logger.info("No sqs messages to process")
        return
    logger.info("processing sqs %d records" % len(event["Records"]))

    region = None
    if context:
        region = context.invoked_function_arn.split(':')[3]
    current_date = datetime.utcnow()
    file_name_prefix = "%s-%s-%s.%d" % ("tmp/tmp-invocation-log", region, current_date.strftime("%Y-%m-%d-%H"), int(current_date.timestamp() * 1000))
    updated_temporary_file = append_and_compress([], event)
    upload_and_cleanup(file_name_prefix, updated_temporary_file, event)


def aggregate_logs(event, context):
    """Lambda to aggregate hourly logs into one file"""

    current_date = datetime.utcnow()
    region = None
    if context:
        region = context.invoked_function_arn.split(':')[3]
    aggregate_old_logs(current_date, region)

def aggregate_old_logs(current_date :datetime, region :str):
    file_name_prefix = "tmp/tmp-invocation-log-%s" % region
    # clean up older files, perhaps from much earlier
    batched_rows, new_keys = search_for_old_log(file_name_prefix, current_date)
    if len(batched_rows) > 0:
        for log_date, rows in batched_rows.items():
            logger.info("aggregating logs for %s" % (log_date))
            aggregated_file = append_and_compress(rows, event=None)
            aggregated_file_prefix = log_date.strftime("logs/%%Y/%%m/%%d/invocation-log-%s-%%Y-%%m-%%d-%%H" % region)
            upload_and_cleanup(aggregated_file_prefix, aggregated_file, event=None)
            try:
                # delete the temporary logs only after successful aggregation
                keys_to_delete = list(filter(lambda x: log_date.strftime("invocation-log-%s-%%Y-%%m-%%d-%%H" % region) in x["Key"], [{'Key': key} for key in new_keys]))
                logger.info("deleting %s" % str(keys_to_delete))
                delete_objects_response = s3_client.delete_objects(
                    Bucket=os.environ["INVOCATION_BUCKET"],
                    Delete={
                        'Objects': keys_to_delete,
                    },
                )
                if 'Errors' in delete_objects_response and len(delete_objects_response['Errors']) > 0:
                    logger.error(delete_objects_response)
                    logger.error("Failed to delete keys")
                    raise RuntimeError("unable to delete keys")
                deleted_keys = set([deleted_key['Key'] for deleted_key in delete_objects_response["Deleted"]])
                checked_count = 0
                for key in keys_to_delete:
                    if key['Key'] not in deleted_keys:
                        message = "Failed to delete key %s" % key['Key']
                        logger.error(message)
                        raise RuntimeError(message)
                    else:
                        checked_count += 1
                if checked_count != len(keys_to_delete):
                    messsage = "Expected to delete %d keys. Only deleted %d" % (len(keys_to_delete), checked_count)
                    logger.error(message)
                    raise RuntimeError(message)
            except botocore.exceptions.ClientError as error:
                logger.error(error.response['Error']['Code'])
                raise error
    else:
        logger.info("no old logs to process")

def search_for_old_log(file_name_prefix: str, current_date: datetime):
    # search for log file in s3
    bucket_name = os.environ["INVOCATION_BUCKET"]

    try:
        list_objects_result = s3_client.list_objects_v2(
            Bucket=bucket_name, 
            Prefix=file_name_prefix
        )
    except botocore.exceptions.ClientError as error:
        logger.error(error.response['Error']['Code'])
        raise error
    logger.info(list_objects_result)
    new_keys = []
    if "Contents" in list_objects_result and len(list_objects_result["Contents"]) > 0:
        logger.info("Found existing invocation file")
        # found a previous log file
        date_batched_rows = {}
        for content in list_objects_result["Contents"]:
            key = None
            k = content["Key"]
            if k[-1] != '/':
                key = k
            else:
                continue
            log_date = datetime.strptime(re.match(r".+(\d{4}-\d{2}-\d{2}-\d{2}).+", key).groups()[0], "%Y-%m-%d-%H")
            current_date_without_minutes = datetime(year=current_date.year, hour=current_date.hour, day=current_date.day, month=current_date.month, minute=0) 
            if log_date >= current_date_without_minutes:
                logger.info("skipping the current hour %s" % (current_date_without_minutes))
                # skip the current hour
                continue
            logger.info("Working on %s" % log_date)
            target_file = tempfile.NamedTemporaryFile(mode="wb+")
            try:
                new_keys.append(key)
                s3_client.download_file(bucket_name, key, target_file.name)
            except botocore.exceptions.ClientError as error:
                logger.error(error.response['Error']['Code'])
                raise error
            # unzip
            rows = []
            with gzip.open(target_file.name, "rb") as f_in:
                for row in csv.DictReader(io.StringIO(f_in.read().decode("utf-8"))):
                    rows.append(row)
            if log_date not in date_batched_rows:
                date_batched_rows[log_date] = rows
            else:
                date_batched_rows[log_date] += rows
        return date_batched_rows, new_keys
    else:
        logger.info("creating a new invocation file")
        # starting a new log file
        return [], new_keys

def append_and_compress(rows, event):
    logger.info("appending to invocation file")
    updated_target_file = tempfile.NamedTemporaryFile(mode="w+")
    if len(rows) == 0:
        fieldnames = set({})
    else:
        fieldnames = set(rows[0].keys())
    if event is not None:
        for record in event["Records"]:
            for key in json.loads(record["body"]).keys():
                fieldnames.add(key)
    output = io.StringIO()
    writer = csv.DictWriter(output, sorted(fieldnames), quoting=csv.QUOTE_NONNUMERIC)
    writer.writeheader()
    for row in rows:
        writer.writerow(row)

    # append to log file
    if event is not None:
        for record in event['Records']:
            if len(record["body"]) > 0:
                writer.writerow(json.loads(record["body"]))

    # zip file and upload log file
    logger.info("compressing invocation file")
    updated_target_file = tempfile.NamedTemporaryFile(mode="w+")
    with gzip.open(updated_target_file.name, 'wb') as f_out:
        f_out.write(output.getvalue().encode("utf-8"))
    return updated_target_file

def upload_and_cleanup(file_name_prefix :str, target_file :tempfile.NamedTemporaryFile, event):
    key = "%s.gz" % file_name_prefix
    try:
        # if s3 fails, the message will reappear in sqs and another attempt will be made to process it
        # so no need to handle fallback scenario
        s3_client.upload_file(target_file.name, os.environ["INVOCATION_BUCKET"], key)
        if event is not None:
            for record in event["Records"]:
                response = sqs_client.delete_message(
                    QueueUrl=os.environ["INVOCATION_QUEUE"],
                    ReceiptHandle=record["receiptHandle"]
                )
    except botocore.exceptions.ClientError as error:
        logger.error(error.response['Error']['Code'])
        logger.error(error)
        raise error