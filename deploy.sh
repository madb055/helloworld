#! /bin/bash

set -e
echo "Deploying service"

[[ -z "$env" ]] && { echo "env parameter is empty" ; exit 1; }
[[ -z "$region" ]] && { echo "region parameter is empty" ; exit 1; }

echo "Deploying env: $env in region: $region"

npm install --silent --no-progress -g npm
npm install --silent --no-progress -g serverless
serverless deploy --stage $env --package $CODEBUILD_SRC_DIR/target/$env -v -r $region