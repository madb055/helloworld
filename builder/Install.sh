#!/bin/bash

# make sure that this entire script fails if any command fails
set -e

echo "Running Install phase `date`"

apt-get update
apt-get -y install python3-pip python3.8-venv


npm install --silent --no-progress -g npm
npm install --silent --no-progress -g serverless



echo "End of Install phase `date`"