#!/bin/bash

# make sure that this entire script fails if any command fails
set -e

echo "Running PostBuild phase `date`"

echo "End of PostBuild phase `date`"