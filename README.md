# Distributed HelloWorld #

This document will serve as the design document for this distributed helloworld project.


### Skills Assessment ###

As a user, I’d like to see how many times the helloworld service has been invoked:

    Create a service which returns helloworld
        Serverless framework
        Lambda
        API Gateway
    Every time the service is called, there is a new record added to the invocation file in S3 
    A QuickSight report is created automatically which displays #times the service is invoked
    Call the service multiple times
    Report should be refreshed automatically with the total number of invocations
    Test the service automatically to make sure it returns helloworld
    Change the code to response with byeworld, should result in deployment rejection

### Constraints, Assumptions & Estimates ###
- Serverless
- API needs to be highly available
- API must be low latency
- Must be scalable and able to handle a high volumne of conourrent requests
- Must inimize data transfer
- Assumes that Rooute53 DNS resolution and host Health Checks are highly reliable.

### Design ###

![Design](design.jpg)


#### Multi-regional Architecture ####
- Distributed API should exist in multiple regions
- When a host/service is unavailable re-route the calls to another available region
- Even distribution of load across all hosts

#### Security Considerations ####
- Implicit throttling provided by using AWS API Gateway (https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-request-throttling.html)
- The API is public. Authentication and Authorization not required at the moment.

### Implementation ####

#### Routing ####
- Route 53 performs frequent Health Checks to find out which hosts are healthy.
- Route 53 uniformly wieghts the requets across the available healthy hosts.
- If a health check fails, the unhealthy host does not receive any further requests until it's health check succeeds. All requests are routed to the healthy hosts instead. (**live demonstration**)
- In a real project, the health checks should do a deep ping on the downstream services used to build the API and therefore give a good indication on the status of the service.

#### Invocation Logging ####
- Use SQS queue to decouple the logging of the invocation from returning a response to the user.
- Putting the message on the SQS queue is sufficient enough to return the repsonse to the user.
- Downstream calls to SQS and S3 are retried on failure with exponential backoff.
- Failure to reach SQS in one region should fallover to another existing queues in other region. (**live demonstration**)

If S3 is not reachable, there is no need to attempt to use alternate regional endpoints since the messages whill not be deleted from the SQS queue and therefore, they will again become visible in the queue and therefore be reprocesssed when S3 is back up.

Have a single lambda worker per region that is triggered by the regional queue to log the invocation in S3.
If S3 is not available for some reason, the message is not deleted from SQS queue and therefore will be retried at a later time when it becomes visible again.
The invocation logs are namespaced by date and region. A sweeper lambda function aggregates them into hourly logs.

These logs are ingested into QuickSight to visualize the number of calls.

### Operational Details ###

- CI/CD via AWS pieplines. A separate pipeline for each region that requires unit tests to pass as well as manual approvals. (**live demonstration**)
- Use multiple stages (development, gamma & prod) and have controlled progression of changes to production.
- Unit testing are included.
- Versioning future proofs the API from changes that might break backwards compatibility
- Separation of configurations for development, gamman and production.

### Telemetry ###

-SQS processing time
- Metrics from Lambda
- Logs from CloudWatch
- QuickSights Dashboard

### Future Improvements ###

- **Availability** - A minimum of 3 different geographical regions should be used. I used only 2 in this projects just to demonstrate the concept.
- **Deduping** - Namespace the log entries using the request time. Also, before an aggregated log is pushed to s3, check to see whether an existing file is going to be overwritten.
If so, do a union on the files using the request id. This will append new requests to the already existing invocation logs.
- **Authentication** - Authenticated APIs if necessary. some options here are API Gateway resource policy, IAM etc
- **Throttling** - Account based throttling via API Gateway
- **Testing** - Integration tests
- **Operational Security** - IAM restrictions so that development environments can only deploy to their development stage and not gamma/prod


### APIs ###
#### Gamma ####
https://api.gamma.staircasedemo.click/v1/hello
https://api.gamma.staircasedemo.click/v1/status


#### Prod ####
https://api.staircasedemo.click/v1/hello
https://api.staircasedemo.click/v1/status




## GLOSSARY ##
### Live Demonstration ###

#### Multi-regional routing fallback ####

https://us-east-2.console.aws.amazon.com/lambda/home?region=us-east-2#/functions/staircase-helloworld-gamma-hello?tab=configure
```bash
for i in `seq 1 100`; do curl https://api.gamma.staircasedemo.click/v1/hello -s | grep "\"hello[^\"]\+\"" -o; done

```
- Run the above command, notice that both regions will handle the requests.
- Add environment variable *STATUS = fail* to lambda in gamma of one region, say us-east-2. This triggers the us-east-2 host to fail the HealthChecks.

https://us-east-2.console.aws.amazon.com/lambda/home?region=us-east-2#/functions/staircase-helloworld-gamma-hello?tab=configure

- Re-run the above command. Notice that the requests stop going to that host in us-east-2 and only us-west-1 will show.


#### SQS Fallback ####

- Before testing, notice matching regions in the s3 regional logs.
- Add environment variable *TEST_SQS_FALLBACK = true* to lambda in gamma of on region, say us-east-2

https://us-east-2.console.aws.amazon.com/lambda/home?region=us-east-2#/functions/staircase-helloworld-gamma-hello?tab=configure

-Make several requests using 

```bash
for i in `seq 1 100`; do curl https://api.gamma.staircasedemo.click/v1/hello -s; done

```
- Notice that the s3 logs that are from us-west-1 will now contain requests from us-east-2 because they were rereouted to the us-west-1 queue. 
