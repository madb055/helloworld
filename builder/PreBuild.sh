#!/bin/bash
# make sure that this entire script fails if any command fails
set -e
echo "Running PreBuild phase `date`"

echo "End of PreBuild phase `date`"